@extends('layouts.app')
@section('content')
    <div class="container mt-2">
        <div class="col-10">
            <h5>Edit comment:</h5>
            <form method="post" action="{{route('posts.comments.update', ['comment' => $comment, 'post' => $post])}}">
                @csrf
                @method('put')
                <div class="form-row">
                    <div class="form-group col-md-10">
                        <label for="body"><b>Comment</b></label><span class="text-danger">*</span>
                        <textarea rows="5" class="form-control @error('body') is-invalid @enderror" id="body"
                                  name="body">{{$comment->body}}</textarea>
                        @error('body')
                        <p class="error">{{$message}}</p>
                        @enderror
                    </div>
                </div>
                <button type="submit" class="btn btn-primary active pl-4 pr-4 mr-3">Edit</button>
                <a href="{{route('posts.show', ['post' => $post])}}">Back</a>
            </form>
        </div>
    </div>
@endsection
