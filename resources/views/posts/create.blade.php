@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="col-6">
            <form method="post" action="{{route('posts.store')}}" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="description"><b>Description</b></label>
                    <textarea name="description" class="form-control @error('description') is-invalid @enderror" id="description"></textarea>
                    @error('description')
                    <p class="error">{{ $message }}</p>
                    @enderror
                </div>
                <div class="form-group">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input  @error('image') is-invalid @enderror" id="customFile" name="image">
                        <label class="custom-file-label" for="customFile">Upload photo</label>
                    </div>
                </div>
                @error('image')
                <p class="error">{{ $message }}</p>
                @enderror
                <button type="submit" class="btn btn-primary active pl-4 pr-4 mr-3">Create</button>
                <a href="{{route('users.index')}}">Back</a>
            </form>
        </div>
    </div>
@endsection
