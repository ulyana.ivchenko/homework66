@extends('layouts.app')
@section('content')
    <div class="container mt-2">
        <div class="col-6">
            <h5>Edit post:</h5>
            <form method="post" action="{{route('posts.update', ['post' => $post])}}" enctype="multipart/form-data">
                @csrf
                @method('put')
                <div class="form-row">
                    <div class="form-group col-md-10">
                        <textarea rows="5" class="form-control @error('description') is-invalid @enderror"
                                  id="description"
                                  name="description">{{$post->description}}</textarea>
                        @error('description')
                        <p class="error">{{$message}}</p>
                        @enderror
                    </div>
                </div>
                <button type="submit" class="btn btn-primary active pl-4 pr-4 mr-3">Edit</button>
                <a href="{{route('users.index')}}">Back</a>
            </form>
        </div>
    </div>
@endsection
