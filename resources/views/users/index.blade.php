@extends('layouts.app')
@section('content')
    <div class="container">
        @if (session('message'))
            <div class="alert alert-primary" role="alert">
                {{ session('message') }}
            </div>
        @endif
        @if(Auth::user())
            <div class="row">
                <h5>Recommended for you</h5>
            </div>
            <div class="row d-flex justify-content-center">
                @foreach($users as $user)
                    <div class="card m-1" style="width: 300px">
                        <div class="media p-3">
                            <img class="d-flex rounded-circle mt-0 mr-3" style="width: 50px; height: 50px"
                                 src="{{asset('/images/default_photo.jpeg')}}" alt="Image">
                            <div class="media-body">
                                <p class="pr-4">{{$user->name}}</p>
                            </div>
                        </div>
                        <div class="card-footer">
                            <a href="{{url('follow', ['user' => $user])}}">Follow</a>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="row d-flex justify-content-center">
                <div>
                    <a href="{{route('posts.create')}}"
                       class="btn btn-info active mt-4" role="button">Create a new post</a>
                </div>
            </div>
            <div class="row d-flex justify-content-center">
                @csrf
                @foreach($posts as $post)
                    @if($post->user->id == Auth::user()->id)
                        <div class="col-md-4 mt-4" id="delete-post-{{$post->id}}">
                            <div class="card ml-0 mr-0">
                                <div class="card-header d-flex justify-content-between">
                                    {{$post->user->name}}
                                </div>
                                <div class="card-body">
                                    <img class="img-fluid"
                                         src="{{asset('/storage/' . $post->image)}}" alt="Photo">
                                    <blockquote class="blockquote mb-0 pt-1">
                                        <p class="text-truncate" style="font-size: 15px;">
                                            {{ $post->description }}
                                        </p>
                                    </blockquote>
                                    <a href="{{route('posts.show', ['post' =>$post])}}"
                                       class="card-link mb-0">Show post</a>
                                    @can('update', $post)
                                        <a href="{{route('posts.edit', ['post' => $post])}}"
                                           class="card-link">Edit post</a>
                                    @endcan
                                    <div class="d-flex justify-content-between pb-0">
                                        @if($post->likes->count() > 0)
                                            <a><i class="fa fa-heart" style="color: indianred"
                                                  aria-hidden="true"></i>
                                                <span style="font-size: 12px"> {{$post->likes->count()}} </span></a>
                                        @endif
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="d-flex justify-content-between">
                                        @if ($post->comments->count() == 1)
                                            <span> {{$post->comments->count()}} Comment</span>
                                        @elseif($post->comments->count() > 1 || $post->comments->count() == 0)
                                            <span> {{$post->comments->count()}} Comments</span>
                                        @endif
                                        @can('delete', $post)
                                            <input type="hidden" id="post-id" value="{{$post->id}}">
                                            <span data-post-id="{{$post->id}}" class="delete"
                                                  aria-hidden="true">&times;</span>
                                        @endcan
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
    </div>
    @endif
@endsection
