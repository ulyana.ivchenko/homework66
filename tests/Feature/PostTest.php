<?php

namespace Tests\Feature;

use App\Post;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Tests\TestCase;

class PostTest extends TestCase
{
    use RefreshDatabase;

    private $user, $posts;

    /**
     *
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->user = factory(User::class)->create();
    }

    /**
     * A basic feature test example.
     * @group post
     * @return void
     */
    public function test_redirect_if_not_auth()
    {
        $response = $this->get('/');
        $response->assertStatus(302);
        $response->assertRedirect('/login');
    }

    /**
     * A basic feature test example.
     * @group post
     * @return void
     */
    public function test_registration()
    {
        $response = $this->get('/register');
        $response->assertStatus(200);
        $response->assertSeeText('Login');
        $response->assertSeeText('Register');
    }

    /**
     * A basic feature test example.
     * @group post
     * @return void
     */
    public function test_login()
    {
        $response = $this->get('/login');
        $response->assertStatus(200);
        $response->assertSeeText('Login');
        $response->assertSeeText('Register');
    }

    /**
     * A basic feature test example.
     * @group post
     * @return void
     */
    public function test_success_get_create_post()
    {
        $this->actingAs($this->user);

        $posts = factory(Post::class, 5)->create([
            'user_id' => $this->user->id,
        ]);

        $response = $this->get(route('posts.create', ['post' => $posts->first()]));
        $response->assertStatus(200);
    }

    /**
     * A basic feature test example.
     * @group post
     * @return void
     */
    public function test_failed_get_edit_not_own_post()
    {
        $another_user = factory(User::class)->create();
        $this->actingAs($this->user);

        $posts = factory(Post::class, 5)->create([
            'user_id' => $another_user->id,
        ]);

        $response = $this->get(route('posts.edit', ['post' => $posts->first()]));
        $response->assertStatus(403);
    }

    /**
     * A basic feature test example.
     * @group post
     * @return void
     */
    public function test_success_edit_own_post()
    {
        $this->actingAs($this->user);

        $post = factory(Post::class)->create([
            'user_id' => $this->user->id,
        ]);

        $data = [
            'description' => 'Test description data',
        ];
        $response = $this->put(route('posts.update', ['post' => $post]), $data);
        $response->assertStatus(302);
        $this->assertDatabaseHas('posts', $data);
    }

    /**
     * A basic feature test example.
     * @group post
     * @return void
     */
    public function test_failed_delete_not_own_post()
    {
        $another_user = factory(User::class)->create();
        $this->actingAs($another_user);

        $posts = factory(Post::class, 5)->create([
            'user_id' => $this->user->id,
        ]);

        $response = $this->delete(route('posts.destroy', ['post' => $posts->first()]));
        $response->assertStatus(403);
    }

    /**
     * A basic feature test example.
     * @group post
     * @return void
     */
    public function test_success_delete_own_post()
    {
        $this->actingAs($this->user);

        $post = factory(Post::class)->create([
            'user_id' => $this->user->id,
        ]);

        $response = $this->delete(route('posts.destroy', ['post' => $post]));
        $response->assertStatus(204);
    }


}
