<?php

namespace Tests\Feature;

use App\Comment;
use App\Post;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CommentTest extends TestCase
{
    use RefreshDatabase;

    private $user, $posts, $comments;

    /**
     *
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->user = factory(User::class)->create();
    }

    /**
     * A basic feature test example.
     * @group comment
     * @return void
     */
    public function test_success_get_comments()
    {
        $this->actingAs($this->user);

        $post = factory(Post::class)->create([
            'user_id' => $this->user->id,
        ]);

        $comment = factory(Comment::class)->create([
            'user_id' => $this->user->id,
            'post_id' => $post->id,
        ]);

        $response = $this->get('/posts/' . $post->id);
        $response->assertStatus(200);

        $response->assertSeeText($comment->body);
        $response->assertSeeText($comment->user->name);
    }

    /**
     * A basic feature test example.
     * @group comment
     * @return void
     */
    public function test_success_create_comment()
    {
        $another_user = factory(User::class)->create();
        $this->actingAs($this->user);

        $post = factory(Post::class)->create([
            'user_id' => $another_user->id,
        ]);

        $data = [
            'body' => 'Test body data',
        ];

        $response = $this->post(route('posts.comments.store', ['post' => $post]), $data);
        $response->assertStatus(201);
        $this->assertDatabaseHas('comments', $data);
    }

    /**
     * A basic feature test example.
     * @group comment
     * @return void
     */
    public function test_success_edit_own_comment()
    {
        $this->actingAs($this->user);

        $post = factory(Post::class)->create([
            'user_id' => $this->user->id,
        ]);

        $comment = factory(Comment::class)->create([
            'user_id' => $this->user->id,
            'post_id' => $post->id,
        ]);

        $data = [
            'body' => 'Test body data',
        ];

        $response = $this->put(route('posts.comments.update', ['post' => $post, 'comment' => $comment]), $data);
        $response->assertStatus(302);
        $this->assertDatabaseHas('comments', $data);
    }

    /**
     * A basic feature test example.
     * @group comment
     * @return void
     */
    public function test_failed_get_edit_not_own_comment()
    {
        $another_user = factory(User::class)->create();
        $this->actingAs($this->user);

        $post = factory(Post::class)->create([
            'user_id' => $another_user->id,
        ]);

        $comment = factory(Comment::class)->create([
            'user_id' => $another_user->id,
            'post_id' => $post->id,
        ]);

        $response = $this->get(route('posts.comments.edit', ['post' => $post, 'comment' => $comment]));
        $response->assertStatus(403);
    }

    /**
     * A basic feature test example.
     * @group comment
     * @return void
     */
    public function test_success_delete_own_comment()
    {
        $this->actingAs($this->user);

        $post = factory(Post::class)->create([
            'user_id' => $this->user->id,
        ]);

        $comment = factory(Comment::class)->create([
            'user_id' => $this->user->id,
            'post_id' => $post->id,
        ]);

        $response = $this->delete(route('posts.comments.destroy', ['post' => $post, 'comment' => $comment]));
        $response->assertStatus(200);
    }

    /**
     * A basic feature test example.
     * @group comment
     * @return void
     */
    public function test_failed_delete_not_own_comment()
    {
        $another_user = factory(User::class)->create();
        $this->actingAs($another_user);

        $post = factory(Post::class)->create([
            'user_id' => $this->user->id,
        ]);

        $comment = factory(Comment::class)->create([
            'user_id' => $this->user->id,
            'post_id' => $post->id,
        ]);

        $response = $this->delete(route('posts.comments.destroy', ['post' => $post, 'comment' => $comment]));
        $response->assertStatus(403);
    }
}
