<?php

namespace Tests\Feature;

use App\Post;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class LikeTest extends TestCase
{
    use RefreshDatabase;

    private $user, $post;

    /**
     *
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->user = factory(User::class)->create();
    }

    /**
     * A basic feature test example.
     * @group like
     * @return void
     */
    public function test_success_follow_user()
    {
        $this->actingAs($this->user);

        $post = factory(Post::class)->create([
            'user_id' => $this->user->id,
        ]);

        $this->user->likes()->toggle($post);

        $response = $this->post(route('posts.likes.store', ['post' => $post]));
        $response->assertStatus(302);
    }
}
