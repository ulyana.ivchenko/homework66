<?php

namespace App\Http\Controllers;

use App\Post;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UsersController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $posts = Post::all();
        $user = Auth::user();
        $followed_users = $user->subscriptions()->get();
        $my_id = $request->user()->id;
        $users = User::select('id', 'name', 'email', 'password', 'email_verified_at', 'remember_token', 'created_at', 'updated_at')
            ->whereNotIn('id', [$my_id])->inRandomOrder()->take(10)->get();
        $diff = $users->diff($followed_users);
        $users = $diff->all();
        return view('users.index', compact('users', 'posts'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function follow(Request $request, $id)
    {
        $user = User::findOrFail($id);
        Auth::user();
        $my_id = $request->user()->id;
        $user->subscribers()->attach($my_id);
        return redirect()->route('posts.index')->with('message', "You are following {$user->name}!");
    }
}
